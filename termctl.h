#pragma once
#include <stdint.h>
struct term {
	using print_t = void (*)(const char*);
	print_t print;
	bool enabled = true;
	struct rule {
		enum {fg,bg,brightness} kind;
		uint8_t value;
		bool c256;
	};
	rule stack[32];
	rule* stacktop = nullptr;
	void _newrule();
	void fg(uint8_t c);
	void rfg(uint8_t c);
	void bg(uint8_t c);
	void rbg(uint8_t c);
	void bt(bool b);
	void drop(uint8_t i = 1);
	void empty();
	enum color_rule {none, basic, rich};
	void _color(color_rule set, uint8_t val, char code);
	void _apply();
};
