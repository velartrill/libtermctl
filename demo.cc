#include "termctl.h"
#include <stdio.h>
void printf_wrapper(const char* i) {
	printf("%s",i);
}
int main() {
	term t;
	t.print = &printf_wrapper;
	t.fg(3);
		printf("hello, my dear ");
		t.bt(true); t.fg(7); t.bg(0);
			printf(" REDACTED ");
		t.drop(3);
		printf("! it is a ");
		t.bt(1); t.rfg(225); t.rbg(25);
			printf("distinct misery");
		t.drop(3);
		printf(" to welcome you to this demonstration.\n");
	t.drop();
}
