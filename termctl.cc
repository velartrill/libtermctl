#include "termctl.h"
#include <unistd.h>
void term::_newrule() {
	if (stacktop == nullptr) stacktop = stack;
		else ++stacktop;
}
void term::fg(uint8_t c) { _newrule(); *stacktop = {rule::fg,c,false}; _apply(); }
void term::rfg(uint8_t c) { _newrule(); *stacktop = {rule::fg,c,true}; _apply(); }
void term::bg(uint8_t c) { _newrule(); *stacktop = {rule::bg,c,false}; _apply(); }
void term::rbg(uint8_t c) { _newrule(); *stacktop = {rule::bg,c,true}; _apply(); }
void term::bt(bool b) { _newrule(); *stacktop = {rule::brightness,(uint8_t)b}; _apply(); }
void term::drop(uint8_t i ) {
	if (stacktop-i < stack) stacktop = nullptr;
		else stacktop -= i;
	_apply();
}
void term::empty() {
	stacktop = nullptr;
	_apply();
}
void term::_color(color_rule set, uint8_t val, char code) {
	if (set == basic) {
		char fmt[] = { '\x1b', '[', code, (char)('0' + val), 'm', 0 };
		print(fmt);
	} else if (set == rich) {
		const uint8_t
			d1 = val / 100,
			d2 = (val - d1*100) / 10,
			d3 = val - (d1*100 + d2*10);
		#define c(x) (char)('0'+x)
		char fmt[] = { '\x1b', '[', code, '8', ';', '5', ';', c(d1),c(d2),c(d3), 'm', 0}; 
		#undef c
		print(fmt);
	}
}
void term::_apply() {
	if (!enabled) return;
	print("\x1b[0m");
	color_rule
		set_fg = none,
		set_bg = none;
	uint8_t fg = 0, bg = 0;
	enum {unset, on, off}
		brightness = unset;
	if (stacktop == nullptr) {
		return;
	}
	for (rule* i = stack; i<stacktop+1; ++i) {
		switch (i->kind) {
			case rule::fg:
				set_fg = (i->c256 ? rich : basic);
				fg = i->value;
				break;
			case rule::bg:
				set_bg = (i->c256 ? rich : basic);
				bg = i->value;
				break;
			case rule::brightness:
				brightness = (i->value ? on : off);
				break;
		}
	}
	_color(set_fg, fg, '3');
	_color(set_bg, bg, '4');
	if (brightness == on) {
		print("\x1b[1m");
	}
}
